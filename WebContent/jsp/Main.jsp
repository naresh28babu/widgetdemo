<html>
<title>Remote Client</title>
<head>
	<script type="text/javascript" src="/lib/jquery-1.11.0.min.js"></script>	
	<script type="text/javascript" src="/lib/handlebars.js"></script>	
	<script type="text/javascript" src="/lib/RingCentral/ringcentral-bundle.js"></script>
	<script type="text/javascript" src="/lib/RingCentral/ringcentral-helpers.js"></script>
	<script type="text/javascript" src="/lib/RingCentral/sip-0.7.5.min.js"></script>
	<script type="text/javascript" src="/lib/RingCentral/ringcentral-web-phone.js"></script>


	<script type="text/javascript" src="https://naresh-2182.csez.zohocorpin.com:9091/Platform/js/widgets/widgetClientInterface.js"></script>	
	<script type="text/javascript" src="/js/handler.js"></script>
	<script type="text/javascript" src="/js/rc.js"></script>
	

	<!-- SDK as per widget Configuration -->


	
	<script type="text/javascript">
	/*
	 * initialize widget with config data
	 */
		$(document).ready(function(){
			WidgetInterface.initialize(Handler.WidgetConfig);
			
			$("#widgetsDiv").on("click", ".toggle-icons", function() {
				var ele = $(this);
				if(ele.hasClass("blueTxt"))
				{
					ele.removeClass("blueTxt")	
				}
				else
				{
					ele.addClass("blueTxt")
				}
			});
		});
	
	
	</script>
</head>

<link rel="stylesheet"  type="text/css" href="/css/style.css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

<body class="loadingDiv">
<div>
	<div id="loadingDiv" style="display:none">
		<div class="loadingBG">&nbsp;</div>
		<div class="loadingText">Loading...</div>
	</div>
	<div class="successMsg" style="display:none"></div>
	<div id="widgetsDiv" class="commonDiv">
	</div>
</div>
<script id='LoginWait' type='text/x-handlebars-template'>
      <span class="loggingInTxt">logging you in..</span>
      <img class="loadingImg" src="/img/loading_wait.gif"/>
</script>
<script id='CallInProgress' type='text/x-handlebars-template'>
      <div class="callerInfo">
        <div class="whiteTxt pT35">
            {{#if Name}} 
                {{Name}} 
            {{else}}
             {{Number}}
            {{/if}}
		</div>
        <div class="whiteTxt" id="callTimer">00:00:00</div>
        <table align="center" class="callActions" border="0">
          <tr>
            <td class="whiteTxt pointer">
                <i class="material-icons toggle-icons" onclick="Handler.MuteUnmute(this)" data-action="mute">volume_off</i>
              
            </td>
            <td class="whiteTxt pointer">
	              <i class="material-icons" onclick="Handler.VolUp()">volume_up</i>
            </td>
            <td class="whiteTxt pointer">
	              <i class="material-icons" onclick="Handler.VolDown()">volume_down</i>           
            </td>

          </tr>
          <tr>
            <td class="whiteTxt pointer">
    	          <i class="material-icons" onclick="Handler.Hold()">message</i>
            </td>
            <td class="whiteTxt pointer">
	              <i class="material-icons toggle-icons" onclick="Handler.HoldUnhold(this)" data-action="hold">pause</i>
            </td>
            <td class="whiteTxt pointer">
              	<i class="material-icons toggle-icons" onclick="Handler.StartStopRecord(this)" data-action="start">voicemail</i>
            </td>
          </tr>
          <tr>
            <td class="whiteTxt pointer"></td>
            <td class="whiteTxt pointer">
	              <i class="material-icons endCallBtn" onclick="Handler.Hangup('{{dataID}}')">call_end</i>
            </td>
            <td class="whiteTxt pointer"></td>
          </tr>
        </table>
      </div>
</script>

<script id='IncomingCall' type='text/x-handlebars-template'>
	<div class="callerInfo">
        <img class="callerImg" src ="/img/caller.jpg"/>
        <div>
          <h3 class="whiteTxt m10 callerName">
            {{#if Name}} 
                {{Name}} 
            {{else}}
             unknown...
            {{/if}}
          </h3>
        </div>
        <div><h3 class="whiteTxt m10">{{Number}}</h3></div>
        <div class="responseBtn">
	        <input class="redBtn whiteTxt fL mL10" type="button" value="Reject" onclick="Handler.Hangup()"/>
            <input class="greenBtn whiteTxt fR mR10" type="button" value="Answer" onclick="Handler.AnswerCall()"/>
        </div>
      </div>
</script>
<script id='Dialer' type='text/x-handlebars-template'>
	<div id="wrapper">
          <div class="dialpad compact">
              <div class="call-info">
                    <p class="f18 caller-name">Dial the number</p>
                    <input class="number" id="dialNumber" value="{{Number}}"></input>
              </div>
              <div class="dials">
                  <ol>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>1</strong></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>2</strong><sup>abc</sup></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>3</strong><sup>def</sup></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>4</strong><sup>ghi</sup></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>5</strong><sup>jkl</sup></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>6</strong><sup>mno</sup></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>7</strong><sup>pqrs</sup></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>8</strong><sup>tuv</sup></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>9</strong><sup>wxyz</sup></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>*</strong></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>0</strong><sup>+</sup></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p><strong>#</strong></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p style="padding-top: 7px;padding-bottom: 10px;"><strong class="actionTxt">Clear</strong></p></li>
                      <li class="digits" onclick="Handler.enterNumber(this)"><p style="padding-top: 7px;padding-bottom: 10px;"><strong class="actionTxt">Delete</strong></p></li>
                      <li class="digits pad-action" onclick="Handler.DialNumber()"><p style="padding-top: 7px;padding-bottom: 10px;><strong class="actionTxt">Call</strong></p></li>
                  </ol>
              </div>
          </div>
      </div>
</script>
<script id='FeedBack' type='text/x-handlebars-template'>
<div style="padding: 10px;">
  <div class="whiteTxt">Provie a Brief Feedback</div>
  	<textarea id="calleeFeedBack" style="margin-top: 17px;height: 70%;width: 100%;"></textarea>
  	<input type="button" value="submit" onclick="Handler.saveNotes('{{dataID}}')" class="greenBtn" style="margin-top: 10px;float: right;">
</div>
</script>
</body>
</html>